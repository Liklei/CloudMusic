import Vue from 'vue'
import Router from 'vue-router'
// page
import Login from '@/components/login/login'
import Guide from '@/components/guide/guide'
import Index from '@/components/index/index'
import Mymusic from '@/components/mymusic/mymusic'
import Friends from '@/components/friends/friends'
import My from '@/components/my/my'
// index child
import Recommend from '@/components/maincomponents/recommend/recommend'
import Musiclist from '@/components/maincomponents/musiclist/musiclist'
import Musicfm from '@/components/maincomponents/musicfm/musicfm'
import Ranking from '@/components/maincomponents/ranking/ranking'
Vue.use(Router)

export default new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'guide',
      component: Guide,
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/index',
      name: 'index',
      redirect: '/index/recommend',
      component: Index,
      children:[
        {
          path: 'recommend',
          name: 'recommend',
          component: Recommend
        },
        {
          path: 'musiclist',
          name: 'musiclist',
          component: Musiclist
        },
        {
          path: 'musicfm',
          name: 'musicfm',
          component: Musicfm
        },
        {
          path: 'ranking',
          name: 'ranking',
          component: Ranking
        },
      ],
      beforeRouteEnter (to, from, next) {
        console.log(to)
        console.log(from)
        console.log(next)
        console.log(this)
      },
      beforeRouteLeave (to, from, next) {
        console.log(to)
        console.log(from)
        console.log(next)
        console.log(this)
      }
    },
    {
      path: '/mymusic',
      name: 'mymusic',
      component: Mymusic
    },
    {
      path: '/friends',
      name: 'friends',
      component: Friends
    },
    {
      path: '/my',
      name: 'my',
      component: My
    }
  ]
})
