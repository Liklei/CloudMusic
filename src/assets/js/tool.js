import { Toast} from 'mint-ui';
import Judge from './judge'
export default{
    setLocalStroage:(name,value)=>{
       !window.localStorage ? Toast('浏览器不支持localStorage') :console.info('浏览器支持localStorage');
       if(!value)
       {
          Toast('值未设置');
          return;
       }
       if(Judge.judgeObjectType(value) === "Object" || Judge.judgeObjectType(value) === "Array")
       { 
           value = JSON.stringify(value);
       }
       window.localStorage.setItem(name,value);
    },
    getLocalStroage:(name)=>{
        !window.localStorage ? Toast('浏览器不支持localStorage') :console.info('浏览器支持localStorage');
        window.setLocalStroage.getItem(name);
    },
    clearStroage:(name)=>{
        !window.localStorage ? Toast('浏览器不支持localStorage') :console.info('浏览器支持localStorage');
        name ? localStorage.removeItem(name) : localStorage.clear();
    },

}