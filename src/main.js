import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource';
import MintUI from 'mint-ui';
import 'mint-ui/lib/style.css';

import '@/assets/css/style.css';

Vue.config.productionTip = false
Vue.use(VueResource);
Vue.use(MintUI);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
}).$mount('#app')
