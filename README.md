

> A Vue.js Music project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

# 描述
技术点 vue2.0 + vue-router + vueX + stylus 
自述   纯属学习vue以及借鉴其他同学的开发技巧经验
For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

